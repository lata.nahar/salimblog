<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Event\EventInterface;



/**
 * Achievements Controller
 *
 * @property \App\Model\Table\AchievementsTable $Achievements
 * @method \App\Model\Entity\Achievement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AchievementsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
     // debug($event);
      //exit;
      $this -> viewBuilder()-> setLayout('back'); 
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */

  

    public function index()
    {
        $achievements = $this->paginate($this->Achievements);

        $this->set(compact('achievements'));
    }

    /**
     * View method
     *
     * @param string|null $id Achievement id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $achievement = $this->Achievements->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('achievement'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $achievement = $this->Achievements->newEmptyEntity();

        if ($this->request->is('post')) {

            $achievement = $this->Achievements->patchEntity($achievement, $this->request->getData());
            // debug($achievement);
            // exit;
            if(!$achievement->getErrors){
                    
                $attachment = $this->request->getData('image_file');
                $name = $attachment->getClientFilename();
                $type = $attachment->getClientMediaType();
                // echo $type;
                // exit;
                if($type == 'image/jpeg' || $type == 'image/png'){
                    //$size = $attachment->getSize();
                
                $tmpName = $attachment->getStream()->getMetadata('uri');
                list($width, $height) = getimagesize($tmpName);
                if($height != 180 && $width != 180){
                    $this->Flash->error(__('The attachment dimention is not currect.'));
                    return $this->redirect(['controller'=>'achievements','action' => 'add']);
                }
                //echo "width: " . $width . "<br />";
                //echo "height: " .  $height;
                //exit();
                $div      = explode('.', $name);
                $file_ext = strtolower(end($div));
                $unique_image = substr(md5(strval(time())), 0, 10).'.'.$file_ext;
 
                $targetPath = 'frontEnd'."/".'images'."/".'achievement'."/".$unique_image;
              // $targetPath = WWW_ROOT.'frontEnd'.DS.'images'.DS.$unique_image;
                if($name){
                    $attachment->moveTo($targetPath);
                }
                //$error = $attachment->getError();
                $achievement-> image = $targetPath; 
                }
                else{
                    $this->Flash->error(__('The attachment format not right.',['key' => 'positive',]));
                    return $this->redirect(['controller'=>'achievements','action' => 'add']);
                }
                
            }else{
                $this->Flash->error(__('The achievement could not be saved.'));
            }
 
 
             if ($this->Achievements->save($achievement)) {
                 $this->Flash->success(__('The achievement has been saved.'));
 
                 return $this->redirect(['action' => 'index']);
             }
             $this->Flash->error(__('The achievement could not be saved. Please, try again.'));
        }
             $this->set(compact('achievement'));

    }


           
    

    /**
     * Edit method
     *
     * @param string|null $id Achievement id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    

    public function edit($id = null)
    {
        $achievement = $this->Achievements->get($id, [
            'contain' => [],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $achievement = $this->Achievements->patchEntity($achievement, $this->request->getData());
            
            //File Upload
            if(!$achievement->getErrors){
                    
                $attachment = $this->request->getData('change_image');
                $name = $attachment->getClientFilename();
                $type = $attachment->getClientMediaType();
                // echo $type;
                // exit;
                if($name){
                    if($type == 'image/jpeg' || $type == 'image/png'){
                        //$size = $attachment->getSize();
                    
                    $tmpName = $attachment->getStream()->getMetadata('uri');
                    list($width, $height) = getimagesize($tmpName);
                    if($height != 180 && $width != 180){
                        $this->Flash->error(__('The attachment dimention is not currect.'));
                        return $this->redirect(['controller'=>'achievements','action' => 'add']);
                    }
                    //echo "width: " . $width . "<br />";
                    //echo "height: " .  $height;
                    //exit();
                    $div      = explode('.', $name);
                    $file_ext = strtolower(end($div));
                    $unique_image = substr(md5(strval(time())), 0, 10).'.'.$file_ext;
     
                    $targetPath = 'frontEnd'."/".'images'."/".'achievement'."/".$unique_image;
                    if($name){
                        $attachment->moveTo($targetPath);
                        unlink($achievement->image);
                    }
                    //$error = $attachment->getError();
                    $achievement-> image = $targetPath; 
                    }
                    else{
                        $this->Flash->error(__('The attachment format not right.'));
                        return $this->redirect(['controller'=>'achievements','action' => 'add']);
                    }
                }
                
                
            }else{
                $this->Flash->error(__('The achievement could not be saved.'));
            }//file upload end



            if ($this->Achievements->save($achievement)) {
                $this->Flash->success(__('The achievement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The achievement could not be saved. Please, try again.'));
        }
        $this->set(compact('achievement'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Achievement id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $achievement = $this->Achievements->get($id);

        $path = $achievement->image;
        
       // exit(file_exists($path));
        //exit;

        if ($this->Achievements->delete($achievement)) {
            if(file_exists($path)){
                unlink($path);
            }
            $this->Flash->success(__('The achievement has been deleted.'));
        } else {
            $this->Flash->error(__('The achievement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
