<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Bgimages Controller
 *
 * @method \App\Model\Entity\Bgimage[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BgimagesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $bgimages = $this->paginate($this->Bgimages);

        $this->set(compact('bgimages'));
    }

    /**
     * View method
     *
     * @param string|null $id Bgimage id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bgimage = $this->Bgimages->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('bgimage'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bgimage = $this->Bgimages->newEmptyEntity();

        if ($this->request->is('post')) {
            $bgimage = $this->Bgimages->patchEntity($bgimage, $this->request->getData());
            if ($this->Bgimages->save($bgimage)) {
                $this->Flash->success(__('The bgimage has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bgimage could not be saved. Please, try again.'));
        }
        $this->set(compact('bgimage'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bgimage id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bgimage = $this->Bgimages->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bgimage = $this->Bgimages->patchEntity($bgimage, $this->request->getData());

            //File Upload
            if(!$bgimage->getErrors){
                    
                $attachment = $this->request->getData('bg_image');
                $name = $attachment->getClientFilename();
                $type = $attachment->getClientMediaType();
                // echo $type;
                // echo $name;
                //  exit;
                if($name){
                    // echo $type;
                    //      exit;
                    if($type == 'image/jpg' || $type == 'image/jpeg' ){
                        $size = $attachment->getSize();
                        // echo $type;
                        //  exit;
                    
                    $tmpName = $attachment->getStream()->getMetadata('uri');
                    list($width, $height) = getimagesize($tmpName);
                    if($height != 700 && $width != 1446){
                        // echo "width: " . $width . "<br />";
                        // echo "height: " .  $height;
                        // exit();
                        $this->Flash->error(__('The attachment dimention is not currect.'));
                        return $this->redirect(['controller'=>'bgimages','action' => 'edit',$bgimage->id]);
                    }
                    
                    $div      = explode('.', $name);
                    $file_ext = strtolower(end($div));
                    $unique_image = 'bgimage'.'.'.'jpg';
     
                    $targetPath = 'frontEnd'."/".'images'."/".'bg'."/".$unique_image;
                    if($name){
                        $attachment->moveTo($targetPath);
                        unlink($bgimage->image);
                    }
                    //$error = $attachment->getError();
                    $bgimage-> image = $targetPath; 
                    }
                    else{
                        $this->Flash->error(__('The attachment format not right.'));
                        return $this->redirect(['controller'=>'bgimages','action' => 'edit',$bgimage->id]);
                    }
                }
                
                
            }else{
                $this->Flash->error(__('The achievement could not be saved.'));
            }//file upload end

            if ($this->Bgimages->save($bgimage)) {
                $this->Flash->success(__('The bgimage has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bgimage could not be saved. Please, try again.'));
        }
        $this->set(compact('bgimage'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bgimage id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bgimage = $this->Bgimages->get($id);
        if ($this->Bgimages->delete($bgimage)) {
            $this->Flash->success(__('The bgimage has been deleted.'));
        } else {
            $this->Flash->error(__('The bgimage could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
