<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BgimagesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BgimagesTable Test Case
 */
class BgimagesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BgimagesTable
     */
    protected $Bgimages;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Bgimages',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Bgimages') ? [] : ['className' => BgimagesTable::class];
        $this->Bgimages = $this->getTableLocator()->get('Bgimages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Bgimages);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
