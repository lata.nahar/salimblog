 <!-- Home -->
 <section class="bg-home-img height-100" id="home">
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 text-left">
                            <h6 style="visibility: visible; animation-name: fadeIn;">
                                <p class="home-title line-height-9 text-white poppins-semi-bold wow fadeIn" style="padding-top:200px">Welcome to the office of<br>
								Mohammad Solaiman Salim</p> </h6>
                           
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="height-80" id="home">
 
 <ul class="home-nav">
        <li>
             <a href="about">
             <div class="container">	
             Read about Solaiman's life							</div>
                     				
             </a>
         </li>
                     <li>
             <a href="event">
                 <div class="container">	
                     See Solaiman's current projects						</div>
             </a>
         </li>
                     <li>
             <a href="home">
                 <div class="container">	
                     Learn about Solaiman's vision for Dhaka-7						</div>
             </a>
         </li>
                     <li>
             <a href="contact">
                 <div class="container">	
                     Send Solaiman a note</div>
             </a>
         </li>
             </ul>

</section>

    <!-- Video start -->
	
    <section class="video-wrapper d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center wow fadeIn" data-wow-delay="400ms"
                     style="visibility: visible; animation-delay: 400ms; animation-name: fadeIn;">

                    <a id="play-video" class="video-play-button" data-fancybox=""
                       href="#">
                        <span></span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- Video end -->
    <!---hide content start-->

<!--hide content-->

    <!-- Social start -->
    
    <section id="socialFeed-row" class="section-alt bg-cover">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="BebasNeue text-blue title">
                        <i class="mdi mdi-rss"></i> Social Feeds</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12  wow fadeIn" data-wow-delay="300ms"
                     style="visibility: visible; animation-delay: 300ms; animation-name: fadeIn;">
                    <div class="owl-carousel owl-theme owl-loaded owl-drag">
                        <div class="owl-stage-outer">
                            <div class="owl-stage"
                                 style="transform: translate3d(-2817px, 0px, 0px); transition: all 0.25s ease 0s; width: 5565px; padding-left: 200px; padding-right: 200px;">
                                         <div class="owl-item" style="width: 449.5px; margin-right: 20px;">
                                            <div class="item">
                                                <div class="cardbox bg-white shadow-sm rounded">
                                                    <div class="cardbox-heading">
                                                        <div class="media m-0">
                                                            <div class="d-flex mr-3">
                                                                <a href="#">
                                                                    <img class="img-fluid rounded-circle"
                                                                         src="frontEnd/images/ss.jpg"
                                                                         alt="User">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <h5 class="montserrat line-height-9 text-blue">
                                                                    Solaiman Salim</h5>
                                                                <small><span><i
                                                                            class="icon ion-md-time"></i> 11 hours ago</span></small>

                                                            </div>
                                                        </div><!--/ media -->
                                                    </div><!--/ cardbox-heading -->

                                                    <div class="cardbox-item">
                                                        <p class="comment more text-break">
                                                                                                                            
                                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                                                                                    </p>
                                                        <a href="#" target="_blank">
                                                        <iframe src="https://www.facebook.com/plugins/video.php?height=314&href=https%3A%2F%2Fwww.facebook.com%2Fzapalak%2Fvideos%2F3827634570652711%2F&show_text=true&width=560" width="560" height="429" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>

                                             <!-- <img class="img-fluid social-img"
                                  src="../scontent.xx.fbcdn.net/v/t1.0-9/163064564_4000842819954245_3320351575313015540_n5b82.jpg?_nc_cat=105&amp;ccb=1-3&amp;_nc_sid=110474&amp;_nc_eui2=AeH533VElfoeLPbyKo5CBNi1vM6z1Hj94tO8zrPUeP3i03n6Lh2lVwaYQi_BBP-8dks&amp;_nc_ohc=c60zSPpZilkAX-4fndw&amp;_nc_ht=scontent.xx&amp;oh=0b518e81cb05f12aa30fb0fd3e916758&amp;oe=607B6678"
                                                                 alt="Image"> -->
                                                        </a>
                                                    </div>
                                                </div><!--/ cardbox -->
                                            </div>
                                        </div>
                                        
                                  </div>
                        </div>
                        <div class="owl-nav disabled">
                            <button type="button" role="presentation" class="owl-prev"><span
                                    aria-label="Previous">‹</span></button>
                            <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Social Feed End -->
    
    <!--counter -->
    
    <!--funfact end-->

    <!-- Achievements -->
    <section id="involved-row" class="section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="BebasNeue text-blue title">
                        Achievements</h3>
                </div>
            </div>
            <div class="row text-center gutter-6">
            <?php
                    foreach ($achievement as $key => $achieve) {

                    if($achieve->status == "active"){
            ?>
                <div class="col-lg-3 col-md-6 col-6 involved-pb wow slideInLeft"
                     style="visibility: visible; animation-name: slideInLeft;">
                    <div class="card shadow rounded involved border-0">
                        <img src="<?php echo $achieve->image?>"
                             class="card-img-top achievement-img"
                             alt="...">
                        <div class="card-body involved-body">
                            <h5 class="card-title montserrat-bold"><?php echo $achieve->title ?></h5>
                            <p class="card-text montserrat"><?php echo $achieve->description ?></p>
                        </div>
                    </div>
                </div>
                        <?php
                    } 
                    }
                    ?>
                <!-- <div class="col-lg-3 col-md-6 col-6 involved-pb wow slideInLeft"
                     style="visibility: visible; animation-name: slideInLeft;">
                    <div class="card shadow rounded involved border-0">
                        <img src="frontEnd/icons/abc.png"
                             class="card-img-top achievement-img"
                             alt="...">
                        <div class="card-body involved-body">
                            <h5 class="card-title montserrat-bold">Lorem Ipsum is simply dummy text</h5>
                            <p class="card-text montserrat">Lorem Ipsum is simply dummy text</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-6 involved-pb wow slideInLeft"
                     style="visibility: visible; animation-name: slideInLeft;">
                    <div class="card shadow rounded involved border-0">
                        <img src="frontEnd/icons/abc.png"
                             class="card-img-top achievement-img"
                             alt="...">
                        <div class="card-body involved-body">
                            <h5 class="card-title montserrat-bold">Lorem Ipsum is simply dummy text</h5>
                            <p class="card-text montserrat">Lorem Ipsum is simply dummy text</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-6 involved-pb wow slideInLeft"
                     style="visibility: visible; animation-name: slideInLeft;">
                    <div class="card shadow rounded involved border-0">
                        <img src="frontEnd/icons/abc.png"
                             class="card-img-top achievement-img"
                             alt="...">
                        <div class="card-body involved-body">
                            <h5 class="card-title montserrat-bold">Lorem Ipsum is simply dummy text</h5>
                            <p class="card-text montserrat">Lorem Ipsum is simply dummy text</p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!--Achievements end -->


    <div class="bg-light">
        
        <div class="ex" data-flickity='{ "wrapAround": true,"pageDots":false,"fade":true,"autoPlay":true }'>
                            <img class="carousel-cell" src="uploads/photo/1.jpg" alt="orange tree"/>
                            <img class="carousel-cell" src="uploads/photo/2.jpg" alt="orange tree"/>
                            <img class="carousel-cell" src="uploads/photo/3.jpg" alt="orange tree"/>
                           
                    </div>
        <div class="text-center mt-3 pb-4">
            <a class="btn btn-sm btn-outline-blue montserrat-bold wow fadeIn" data-wow-delay="800ms"
               href="photos"
               style="visibility: visible; animation-delay: 800ms; animation-name: fadeIn;">
                More Photos
                <span class="mdi mdi-arrow-right-bold"></span>
            </a>
        </div>
    </div>

    <!-- Media Coverage -->
    <section class="blog-area section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="BebasNeue text-blue title">
                        <i class="mdi mdi-newspaper"></i> Media Coverage</h3>
                </div>
            </div>
            <div class="row gutter-6">
                        <div class="col-lg-3 col-md-6 col-6">
                            <div class="single-blog-post-item">
                                <div class="post-image">
                                    <a href="#"
                                       target="_self" class="news-thumbnail">
                                        <img src="uploads/media/IOsDDWrNeBaHIzOYtHOK.jpg"
                                             class="img-fluid"
                                             alt="image">
                                    </a>
                                </div>

                                <div class="post-content">

                                    <ul class="post-meta montserrat-bold">
                                        <li>
                                            <img src="uploads/favicon/WbgdOxAKQRQkKHZIuYNj.png"
                                                     class="img-fluid"
                                                     style="height: 18px; width: 18px; object-fit: contain;" alt="">
                                                                                        &nbsp;<a
                                                href="#"
                                                target="_self">Bangladesh Awami League</a></li>
                                        

                                    </ul>
                                    <h3><a href="javascript:void(0)" class="text-dark text-break">Lorem Ipsum is simply dummy text of the printing</a>
                                    </h3>
                                    <a href="#"
                                       target="_self"
                                       class="read-more-btn montserrat-bold">Read More <i
                                            class="mdi mdi-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                           <div class="col-lg-3 col-md-6 col-6">
                            <div class="single-blog-post-item">
                                <div class="post-image">
                                    <a href="#"
                                       target="_self" class="news-thumbnail">
                                        <img src="uploads/media/IOsDDWrNeBaHIzOYtHOK.jpg"
                                             class="img-fluid"
                                             alt="image">
                                    </a>
                                </div>

                                <div class="post-content">

                                    <ul class="post-meta montserrat-bold">
                                        <li>
                                            <img src="uploads/favicon/WbgdOxAKQRQkKHZIuYNj.png"
                                                     class="img-fluid"
                                                     style="height: 18px; width: 18px; object-fit: contain;" alt="">
                                                                                        &nbsp;<a
                                                href="#"
                                                target="_self">Bangladesh Awami League</a></li>
                                        

                                    </ul>
                                    <h3><a href="javascript:void(0)" class="text-dark text-break">Lorem Ipsum is simply dummy text of the printing</a>
                                    </h3>
                                    <a href="#"
                                       target="_self"
                                       class="read-more-btn montserrat-bold">Read More <i
                                            class="mdi mdi-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-6">
                            <div class="single-blog-post-item">
                                <div class="post-image">
                                    <a href="#"
                                       target="_self" class="news-thumbnail">
                                        <img src="uploads/media/IOsDDWrNeBaHIzOYtHOK.jpg"
                                             class="img-fluid"
                                             alt="image">
                                    </a>
                                </div>

                                <div class="post-content">

                                    <ul class="post-meta montserrat-bold">
                                        <li>
                                            <img src="uploads/favicon/WbgdOxAKQRQkKHZIuYNj.png"
                                                     class="img-fluid"
                                                     style="height: 18px; width: 18px; object-fit: contain;" alt="">
                                                                                        &nbsp;<a
                                                href="#"
                                                target="_self">Bangladesh Awami League</a></li>
                                        

                                    </ul>
                                    <h3><a href="javascript:void(0)" class="text-dark text-break">Lorem Ipsum is simply dummy text of the printing</a>
                                    </h3>
                                    <a href="#"
                                       target="_self"
                                       class="read-more-btn montserrat-bold">Read More <i
                                            class="mdi mdi-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-6">
                            <div class="single-blog-post-item">
                                <div class="post-image">
                                    <a href="#"
                                       target="_self" class="news-thumbnail">
                                        <img src="uploads/media/IOsDDWrNeBaHIzOYtHOK.jpg"
                                             class="img-fluid"
                                             alt="image">
                                    </a>
                                </div>

                                <div class="post-content">

                                    <ul class="post-meta montserrat-bold">
                                        <li>
                                            <img src="uploads/favicon/WbgdOxAKQRQkKHZIuYNj.png"
                                                     class="img-fluid"
                                                     style="height: 18px; width: 18px; object-fit: contain;" alt="">
                                                                                        &nbsp;<a
                                                href="#"
                                                target="_self">Bangladesh Awami League</a></li>
                                        

                                    </ul>
                                    <h3><a href="javascript:void(0)" class="text-dark text-break">Lorem Ipsum is simply dummy text of the printing</a>
                                    </h3>
                                    <a href="#"
                                       target="_self"
                                       class="read-more-btn montserrat-bold">Read More <i
                                            class="mdi mdi-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>                 
                                            
                                            
                                    
                <div class="col-lg-12">
                    <div class="text-center">
                        <a class="btn btn-sm btn-outline-blue montserrat-bold wow fadeIn" data-wow-delay="800ms"
                           href="news"
                           style="visibility: visible; animation-delay: 800ms; animation-name: fadeIn;">
                            More News
                            <span class="mdi mdi-arrow-right-bold"></span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Media Coverage-->
<!-- Footer -->
<style>
    .footer-social li a i {
        border-radius: 2px;
        margin-right: -5px;
    }
    @media (max-width: 768px){
        .center{
            text-align: center !important;
        }
    }
</style>

    <!-- Initiatives -->
    <section class="section-50 bg-blue" id="initiatives">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-12 align-self-center text-white">
                    <div class="pillar-desc">
                        <h3 class="BebasNeue wow fadeIn text-white"
                            style="visibility: visible; animation-name: fadeIn;">Initiatives</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to 
                        </p>
                    </div>
                </div>
                <!-- col end -->
                <div class="col-lg-8 col-md-12 col-12">
                    <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
                        <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                        class="active"></li>
                                                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"
                                        class=""></li>
                                                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"
                                        class=""></li>
                                                                                    </ol>
                        <div class="carousel-inner">
                                <div class="carousel-item active">
                                        <div class="row">
                                            <div class="col-md-6 col-6 pb-2">
                                                    <div
                                                        class="card mb-3 bg-transparent border-0 shadow initiatives-box">
                                                        <div class="row no-gutters">
                                                            <div
                                                                class="col-md-4 align-self-center text-center bg-white">
                                                                <img
                                                                    src="uploads/initiative/abc.png"
                                                                    class="img-fluid ini-img" alt="">
                                                            </div>
                                                            <div class="col-md-8 align-self-center pl-3 pr-1">
                                                                <a href="#" target="_blank"><h2
                                                                        class="BebasNeue font-25 text-white pt-2 font-16px">
                                                                        abc</h2>
                                                                </a>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col-md-6 col-6 pb-2">
                                                    <div
                                                        class="card mb-3 bg-transparent border-0 shadow initiatives-box">
                                                        <div class="row no-gutters">
                                                            <div
                                                                class="col-md-4 align-self-center text-center bg-white">
                                                                <img
                                                                    src="uploads/initiative/abc.png"
                                                                    class="img-fluid ini-img" alt="">
                                                            </div>
                                                            <div class="col-md-8 align-self-center pl-3 pr-1">
                                                                <a href="#" target="_blank"><h2
                                                                        class="BebasNeue font-25 text-white pt-2 font-16px">
                                                                        abc</h2>
                                                                </a>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                      <div class="col-md-6 col-6 pb-2">
                                                    <div
                                                        class="card mb-3 bg-transparent border-0 shadow initiatives-box">
                                                        <div class="row no-gutters">
                                                            <div
                                                                class="col-md-4 align-self-center text-center bg-white">
                                                                <img
                                                                    src="uploads/initiative/abc.png"
                                                                    class="img-fluid ini-img" alt="">
                                                            </div>
                                                            <div class="col-md-8 align-self-center pl-3 pr-1">
                                                                <a href="#" target="_blank"><h2
                                                                        class="BebasNeue font-25 text-white pt-2 font-16px">
                                                                        abc</h2>
                                                                </a>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                     
                                                <div class="col-md-6 col-6 pb-2">
                                                    <div
                                                        class="card mb-3 bg-transparent border-0 shadow initiatives-box">
                                                        <div class="row no-gutters">
                                                            <div
                                                                class="col-md-4 align-self-center text-center bg-white">
                                                                <img
                                                                    src="uploads/initiative/abc.png"
                                                                    class="img-fluid ini-img" alt="">
                                                            </div>
                                                            <div class="col-md-8 align-self-center pl-3 pr-1">
                                                                <a href="#" target="_blank"><h2
                                                                        class="BebasNeue font-25 text-white pt-2 font-16px">
                                                                        abc</h2>
                                                                </a>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="carousel-item ">
                                        <div class="row">
                                            <div class="col-md-6 col-6 pb-2">
                                                    <div
                                                        class="card mb-3 bg-transparent border-0 shadow initiatives-box">
                                                        <div class="row no-gutters">
                                                            <div
                                                                class="col-md-4 align-self-center text-center bg-white">
                                                                <img
                                                                    src="uploads/initiative/abc.png"
                                                                    class="img-fluid ini-img" alt="">
                                                            </div>
                                                            <div class="col-md-8 align-self-center pl-3 pr-1">
                                                                <a href="#" target="_blank"><h2
                                                                        class="BebasNeue font-25 text-white pt-2 font-16px">
                                                                        abc</h2>
                                                                </a>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col-md-6 col-6 pb-2">
                                                    <div
                                                        class="card mb-3 bg-transparent border-0 shadow initiatives-box">
                                                        <div class="row no-gutters">
                                                            <div
                                                                class="col-md-4 align-self-center text-center bg-white">
                                                                <img
                                                                    src="uploads/initiative/abc.png"
                                                                    class="img-fluid ini-img" alt="">
                                                            </div>
                                                            <div class="col-md-8 align-self-center pl-3 pr-1">
                                                                <a href="#" target="_blank"><h2
                                                                        class="BebasNeue font-25 text-white pt-2 font-16px">
                                                                        abc</h2>
                                                                </a>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                      <div class="col-md-6 col-6 pb-2">
                                                    <div
                                                        class="card mb-3 bg-transparent border-0 shadow initiatives-box">
                                                        <div class="row no-gutters">
                                                            <div
                                                                class="col-md-4 align-self-center text-center bg-white">
                                                                <img
                                                                    src="uploads/initiative/abc.png"
                                                                    class="img-fluid ini-img" alt="">
                                                            </div>
                                                            <div class="col-md-8 align-self-center pl-3 pr-1">
                                                                <a href="#" target="_blank"><h2
                                                                        class="BebasNeue font-25 text-white pt-2 font-16px">
                                                                        abc</h2>
                                                                </a>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                     
                                                <div class="col-md-6 col-6 pb-2">
                                                    <div
                                                        class="card mb-3 bg-transparent border-0 shadow initiatives-box">
                                                        <div class="row no-gutters">
                                                            <div
                                                                class="col-md-4 align-self-center text-center bg-white">
                                                                <img
                                                                    src="uploads/initiative/abc.png"
                                                                    class="img-fluid ini-img" alt="">
                                                            </div>
                                                            <div class="col-md-8 align-self-center pl-3 pr-1">
                                                                <a href="#" target="_blank"><h2
                                                                        class="BebasNeue font-25 text-white pt-2 font-16px">
                                                                        abc</h2>
                                                                </a>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                                                   
                                                            
                        </div>
                    </div>

                </div>
                <!-- col end -->
            </div>
            <!-- row end -->
        </div>
        <!-- container end -->
    </section>
    <!-- Initiatives End -->