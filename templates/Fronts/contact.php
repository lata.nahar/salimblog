<section class="section header-img"
             style="background-image: url(uploads/header/LQmPvjzloQqcaDCtWrUOTh.png)">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 page-next-level text-center">
                    <h4 class="pt-3 BebasNeue">Contact</h4>
                    <div class="page-next text-light"><a href="index.html">Home</a>/<span>Contact</span></div>
                </div>
            </div>
        </div>
    </section>
 
 <!-- CONTACT US START -->
 <section class="section" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-heading mb-5">
                        <h3 class="text-dark mb-1 font-weight-light BebasNeue text-uppercase">Get in touch</h3>
                        <div class="title-border-simple position-relative"></div>
                    </div>
                </div>
                <!-- col end -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="contact-box">
                        <div class="row">
                            <div class="col-lg-8 col-md-6">
                                                                <div class="custom-form p-3">
                                    <div id="message"></div>
                                    <form method="post" action="https://palak.net.bd/contact" name="contact-form"
                                          id="contact-form"><input type="hidden" name="_token" value="IYRE2DQQ8ZDddeV2cbgVEwfxuJfIzupzdmRNPCnl">                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group app-label">
                                                    <input name="name" value="" id="name" type="text"
                                                           class="form-control "
                                                           placeholder="Name">
                                                                                                    </div>
                                            </div>
                                            <!-- col end -->
                                            <div class="col-lg-6">
                                                <div class="form-group app-label">
                                                    <input name="email" id="email" value="" type="email" class="form-control "
                                                           placeholder="Email">
                                                                                                    </div>
                                            </div>
                                            <!-- col end -->
                                            <div class="col-lg-12">
                                                <div class="form-group app-label">
                                                    <input type="text" value="" name="subject" class="form-control " id="subject"
                                                           placeholder="Subject"/>
                                                                                                    </div>
                                            </div>
                                            <!-- col end -->
                                            <div class="col-lg-12">
                                                <div class="form-group app-label">
                                                    <textarea name="message" id="message" rows="5"
                                                              class="form-control " placeholder="Message"></textarea>
                                                                                                    </div>
                                            </div>
                                            <!-- col end -->
                                        </div>
                                        <!-- row end -->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="submit" id="submit" name="send"
                                                       class="submitBnt btn btn-custom" value="Send Message">
                                                <div id="simple-msg"></div>
                                            </div>
                                            <!-- col end -->
                                        </div>
                                        <!-- row end -->
                                    </form>
                                </div>
                            </div>
                            <!-- col end -->

                            <div class="col-lg-4 col-md-6">
                                <div class="contact-cantent p-3">
                                    <div class="contact-details">
                                        <div class="float-left contact-icon mr-3 mt-2">
                                            <i class="mdi mdi-headphones text-muted h5"></i>
                                        </div>
                                        <div class="app-contact-desc text-muted pt-1">
                                            <p class="mb-0 info-title f-13">Call :</p>
                                            <p></p>
                                            <?php
                                                foreach ($phonenumber as $key => $num) {
                                                    # code...
                                              
                                            ?>
                                            <p class="mb-0 f-13"><a href="tel:<?php echo $num->number ?>" class="text-dark text-muted"><?php echo $num->number ?></a></p>
                                            <?php
                                                  }

                                            ?>
                                        </div>
                                    </div>

                                    <div class="contact-details mt-2">
                                        <div class="float-left contact-icon mr-3 mt-2">
                                            <i class="mdi mdi-email-outline text-muted h5"></i>
                                        </div>
                                        <div class="app-contact-desc text-muted pt-1">
                                            <p class="mb-0 info-title f-13">Email :</p>
                                            <p></p>
                                            <?php
                                                foreach ($emailaddress as $key => $email) {
                                                    # code...
                                              
                                            ?>
                                            <p class="mb-0 f-13">
                                                <a href="mailto:<?php echo $email->address ?>" class="text-muted"><span class="__cf_email__" data-cfemail="">[email&#160;protected]</span><?php echo $email->address ?></a>
                                            </p> 
                                            <?php
                                                  }

                                            ?>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="contact-details mt-2">
                                        <div class="float-left contact-icon mr-3 mt-2">
                                            <i class="mdi mdi-map-marker text-muted h5"></i>
                                        </div>
                                        <div class="app-contact-desc text-muted pt-1">
                                            <p class="mb-0 info-title f-13">Location :</p>
                                            <p class="mb-0 f-13"><a href="#" class="text-muted">abc</a></p>
                                        </div>
                                    </div>

                                    <div class="follow mt-4">
                                        <h4 class="text-dark mb-3 BebasNeue">Follow</h4>
                                        <ul class="follow-icon list-inline mt-32 mb-0">
                                            <li class="list-inline-item f-15"><a href="#" target="_blank"
                                                                                 class="social-icon text-muted"><i
                                                        class="mdi mdi-facebook"></i></a></li>
                                            <li class="list-inline-item f-15"><a href="#" target="_blank"
                                                                                 class="social-icon text-muted"><i
                                                            class="mdi mdi-instagram"></i></a></li>
                                            <li class="list-inline-item f-15"><a href="#" target="_blank"
                                                                                 class="social-icon text-muted"><i
                                                        class="mdi mdi-twitter"></i></a></li>
                                            <li class="list-inline-item f-15"><a href="#" target="_blank"
                                                                                 class="social-icon text-muted"><i
                                                        class="mdi mdi-youtube-play"></i></a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- col end -->
                        </div>
                        <!-- row end -->
                    </div>
                </div>
                <!-- col end -->
            </div>
            <!-- row end -->
        </div>
        <!-- container end -->
    </section>
