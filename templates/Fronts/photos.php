<section class="section header-img"
             style="background-image: url(uploads/header/HdhMRSgwJMwyiImRbcrzCo.png)">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 page-next-level text-center">
                    <h4 class="pt-3 BebasNeue">Photos</h4>
                    <div class="page-next text-light"><a href="javascript:void(0)">Media</a>/<span>Photos</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="hero-area">
        <div class="slider-active-stage slider-arrow">
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/1.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/QRVrViTaGLeSeBCveHOfcP.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/gUfieLCNPlwCRDqyuApZVe.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/CKwvFGkbyBeBcckCEYCEsG.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/KnnEUBsXVkhMqKKxyOWjch.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/pfHXxallwSJKpIIQcyuXcZ.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/njJPmGMEnrAZfaPmEDwixT.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/KRyOHFxEpDicdevnLdtwwj.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/OMzUDzIEFAQsMFpnhWSunA.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/jibYUieYHZbFHzSSlNrnIu.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/rwxSWXVvEkzNKfRYLUEJzC.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/sOqaODPxnHHLBTrRwiBaOY.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/UgkgZQUGaPESYHjIhhMPtM.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/aiNeLQwzKlmIUxLFDeRyLa.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/tUVPmdzcIdPYDRIwvvyeGf.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/YUzMNnUmtqVZRenqIzryxW.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/VVWSeghdDwxdJpVbqxhSdk.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/NXhrSaUlgjevWfQHMyFhbI.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/rYoCCgaBRtGqyUWAzJEHbT.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/fRDRrApPmJdEBoiHMoUVbD.jpg)"></div>
                            <div class="single-slider slide-height d-flex align-items-end" data-overlay="dark-gradient"
                     style="background-image: url(uploads/photo/EkcdQqHsqqzKQiNWDaLiaS.jpg)"></div>
                    </div>
    </section>
    <div class="album section">
        <div class="container">
            <div class="row gutter-6">
                        <div class="col-lg-3 mb-15 col-md-4 col-6">
                            <div class="card shadow-sm border-0 h-100">
                                <img class="card-img-top"
                                     alt="palak"
                                     src="uploads/album/OiNwIveOlMsMHvckYwsZSO.jpg">
                                <div class="card-body">
                                    <h6 class="card-title montserrat-bold">Lorem Ipsum is simply dummy text</h6>
                                    <p class="card-text">Lorem Ipsum is simply dummy text</p>
                                </div>
                                <div class="card-footer bg-white">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <a class="card-link"
                                           href="more-photos/album2.html"
                                           style="visibility: visible; animation-delay: 700ms; animation-name: fadeIn;">
                                            View Album
                                            <span class="mdi mdi-arrow-right-bold"></span>
                                        </a> <small class="text-muted">8 months ago</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                                            <div class="col-lg-3 mb-15 col-md-4 col-6">
                            <div class="card shadow-sm border-0 h-100">
                                <img class="card-img-top"
                                     alt="palak"
                                     src="uploads/album/OiNwIveOlMsMHvckYwsZSO.jpg">
                                <div class="card-body">
                                    <h6 class="card-title montserrat-bold">Lorem Ipsum is simply dummy text</h6>
                                    <p class="card-text">Lorem Ipsum is simply dummy text</p>
                                </div>
                                <div class="card-footer bg-white">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <a class="card-link"
                                           href="more-photos/album1.html"
                                           style="visibility: visible; animation-delay: 700ms; animation-name: fadeIn;">
                                            View Album
                                            <span class="mdi mdi-arrow-right-bold"></span>
                                        </a> <small class="text-muted">8 months ago</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                           <div class="col-lg-3 mb-15 col-md-4 col-6">
                            <div class="card shadow-sm border-0 h-100">
                                <img class="card-img-top"
                                     alt="palak"
                                     src="uploads/album/OiNwIveOlMsMHvckYwsZSO.jpg">
                                <div class="card-body">
                                    <h6 class="card-title montserrat-bold">Lorem Ipsum is simply dummy text</h6>
                                    <p class="card-text">Lorem Ipsum is simply dummy text</p>
                                </div>
                                <div class="card-footer bg-white">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <a class="card-link"
                                           href="more-photos/album1.html"
                                           style="visibility: visible; animation-delay: 700ms; animation-name: fadeIn;">
                                            View Album
                                            <span class="mdi mdi-arrow-right-bold"></span>
                                        </a> <small class="text-muted">8 months ago</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                           <div class="col-lg-3 mb-15 col-md-4 col-6">
                            <div class="card shadow-sm border-0 h-100">
                                <img class="card-img-top"
                                     alt="palak"
                                     src="uploads/album/OiNwIveOlMsMHvckYwsZSO.jpg">
                                <div class="card-body">
                                    <h6 class="card-title montserrat-bold">Lorem Ipsum is simply dummy text</h6>
                                    <p class="card-text">Lorem Ipsum is simply dummy text</p>
                                </div>
                                <div class="card-footer bg-white">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <a class="card-link"
                                           href="more-photos/album1.html"
                                           style="visibility: visible; animation-delay: 700ms; animation-name: fadeIn;">
                                            View Album
                                            <span class="mdi mdi-arrow-right-bold"></span>
                                        </a> <small class="text-muted">8 months ago</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                                                </div>
        </div>
    </div>