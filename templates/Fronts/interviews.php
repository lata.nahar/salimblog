<section class="section header-img"
             style="background-image: url(uploads/header/BpkxsyxLCueBbZlfiPKnMH.png)">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 page-next-level text-center">
                    <h3 class="pt-3 BebasNeue text-white">Interviews</h3>
                    <div class="page-next text-light"><a href="javascript:void(0)">Media</a>/<span>Interviews</span>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="section">
        <div class="container">
            <div class="row clearfix">
                        <div class="col-lg-4 col-md-4">
                            <div class="card mb-4 border-0 shadow-sm">
                                <div class="thumbnail">
                                    <img src="../i1.ytimg.com/vi/akMlaBQR73c/sddefault.jpg" class="card-img-top"
                                         alt="...">
                                    <a data-fancybox href="https://www.youtube.com/watch?v=akMlaBQR73c" class="btn"><i
                                            class="mdi mdi-play"></i></a>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                </div>
                            </div>
                        </div>
                            <div class="col-lg-4 col-md-4">
                            <div class="card mb-4 border-0 shadow-sm">
                                <div class="thumbnail">
                                    <img src="../i1.ytimg.com/vi/akMlaBQR73c/sddefault.jpg" class="card-img-top"
                                         alt="...">
                                    <a data-fancybox href="https://www.youtube.com/watch?v=akMlaBQR73c" class="btn"><i
                                            class="mdi mdi-play"></i></a>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                </div>
                            </div>
                        </div>        
                                          
                                    
            </div>
        </div>
    </section>

<!-- Footer -->
<style>
    .footer-social li a i {
        border-radius: 2px;
        margin-right: -5px;
    }
    @media (max-width: 768px){
        .center{
            text-align: center !important;
        }
    }
</style>
<section class="bg-blue footer" id="contact">
    <div class="container">
        <div class="row pt-5 gutter-6">
            <div class="col-lg-4 center">
                <h4 class="text-white footer-title BebasNeue ">Related Links</h4>
                <div class="mt-3">
                    <ul class="list-unstyled">
                        <li><a href="#" target="_blank" class="text-white"><i class="mdi mdi-earth"></i> abc</a></li>
                        <li><a href="#" target="_blank" class="text-white"><i class="mdi mdi-earth"></i> xyz</a></li>
                      
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 text-center social-footer">
                <h4 class="text-white footer-title BebasNeue ">Follow My Updates</h4>
                <div class="mt-4">
                    <div class="text-center">
                        <form id="subscribersForm">
                            <div class="input-group mb-3">
                                <input type="email" class="form-control" id="message" placeholder="Enter your email address" name="email">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" id="sendRequest" type="button">Subscribe <i class="mdi mdi-arrow-right-bold"></i></button>
                                </div>
                            </div>
                           
                        </form>
                    </div>
                    <ul class="list-unstyled text-white footer-social list-inline">
                        <li class="list-inline-item"><a href="#" target="_blank"><i class="mdi mdi-facebook facebook"></i></a></li>
                        <li class="list-inline-item"><a href="#" target="_blank"><i class="mdi mdi-instagram instagram"></i></a></li>
                        <li class="list-inline-item"><a href="#" target="_blank"><i class="mdi mdi-twitter twitter"></i></a></li>
                        <li class="list-inline-item"><a href="#" target="_blank"><i class="mdi mdi-youtube-play youtube"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 text-right addr-footer">
                <h4 class="text-white footer-title BebasNeue">Address</h4>
                <div class="mt-4">
                    <address class="montserrat">
                        <p class="address line-height-9 text-white" style="margin-bottom: 0.5rem">abc</p>
                        <p class="address line-height-9 text-white" style="margin-bottom: 0.5rem">xyz</p>
                        <p class="email line-height-9" style="margin-bottom: 0.5rem"><a class="text-white" href=""><a class="text-white" href="index.html" target="_blank">www.SolaimanSalim.net.bd</a></p>
                        <p class="call line-height-9" style="margin-bottom: 0.5rem"><a class="text-white" href="tel:+880-1766-699999">+880-0000-000-000</a></p>
                    </address>
                </div>
            </div>
        </div>
        <div class="footer-border" style="margin-top: 10px;opacity: .2"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left pull-none">
                    <p class="text-white mb-0 montserrat">All rights reserved By <span class="montserrat-bold">Solaiman Salim</span>
                    </p>
                </div>
                <div class="float-right pull-none">
                    <p class="footer-terms text-white">
                        <a href="#.html">Privacy &amp; Policy </a> |
                        <a href="#">Terms &amp; Condition</a> |
                        <a href="contact.html">Contact</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>