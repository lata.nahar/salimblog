<section class="section header-img" style="background-image: url(uploads/header/ykKXJvqZxMWTirtEJVwszq.png)">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 page-next-level text-center">
                    <h4 class="pt-3 BebasNeue text-white">Event</h4>
                    <div class="page-next text-light"><a href="home">Home</a>/<span>Event</span></div>
                </div>
            </div>
        </div>
    </section>

<section class="section">
        <div class="container">
            <div class="row clearfix">
                                                            <div class="col-md-6">
                            <div class="card mb-3 shadow-sm">
                                <div class="row no-gutters">
                                    <div class="col-3 col-md-4 align-self-center">
                                        <img src="uploads/event/event.jpg" class="card-img event-img" alt="...">
                                    </div>
                                    <div class="col-9 col-md-8">
                                        <div class="card-body">
                                            <h5 class="card-title"><a href="http://wcit2021.org.bd/" class="text-dark" target="_blank">Event</a></h5>
                                            <p class="card-text"><span class="mdi mdi-calendar"></span>
                                                   04 April, 2021                                           
                                            <br>
                                                <span class="mdi mdi-map-marker-circle"></span>
                                                Bangladesh </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                       <div class="col-md-6">
                            <div class="card mb-3 shadow-sm">
                                <div class="row no-gutters">
                                    <div class="col-3 col-md-4 align-self-center">
                                        <img src="uploads/event/event.jpg" class="card-img event-img" alt="...">
                                    </div>
                                    <div class="col-9 col-md-8">
                                        <div class="card-body">
                                            <h5 class="card-title"><a href="http://wcit2021.org.bd/" class="text-dark" target="_blank">Event</a></h5>
                                            <p class="card-text"><span class="mdi mdi-calendar"></span>
                                                   04 April, 2021                                           
                                            <br>
                                                <span class="mdi mdi-map-marker-circle"></span>
                                                Bangladesh </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                    
            </div>
        </div>
    </section>