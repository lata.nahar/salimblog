    <!-- home start -->
    <section class="section header-img" style="background-image: url(uploads/header/hZkZiAfPdwcpFNCVdekOYu.png)">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 page-next-level text-center">
                    <h4 class="pt-3 BebasNeue">About Me</h4>
                    <div class="page-next text-light"> <?= $this->Html->link(
    'Home',
    ['controller' => 'fronts', 'action' => 'home', '_full' => true]
);?>/<span>About</span></div>
                </div>
                <div class="under-line">
                </div>
            </div>
        </div>
    </section>
    <!-- home end -->

    <!-- about-->
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="card mb-3 border-0 shadow" style="border-radius: 0">
                    <div class="row no-gutters">
                        <div class="col-md-12 col-lg-4">
                            <img src="frontEnd/images/dmd.png" class="card-img about-pp" alt="">
                        </div>
                        <div class="col-md-12 col-lg-8">
                            <div class="card-body">
                                <h5 class="card-title montserrat-bold text-uppercase  center-text">Solaiman Salim</h5>
                                <p class="text-blue center-text">Managing Director of Madina Group
                                </p>
                                <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                </p>
                                
                                <div class="team-icon">
                                    <ul>
                                        <li>
                                            <a href="#" target="_blank">
                                                <i class="mdi mdi-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                           <a href="#" target="_blank">
                                                <i class="mdi mdi-instagram" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank">
                                                <i class="mdi mdi-twitter" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                          <a href="#" target="_blank">
                                                <i class="mdi mdi-youtube-play"></i>
                                            </a>
                                        </li>


                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end about -->
    <section class="section bg-gray resume">
        <div class="container">
            <div class="row">
                <div class="col-lg-12  text-justify text-center">
                    <div class="section-title pb-2">
                        <h3 class="BebasNeue text-uppercase text-blue wow fadeIn"
                            style="visibility: visible; animation-name: fadeIn;">Biography</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-lg-6">
                    <h3 class="resume-title">Position Held</h3>
                    <div class="resume-item pb-0">
                        <ul>
                            <li class="list-li">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. </li>
                            <li class="list-li">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. </li>
                            <li class="list-li">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. </li>
                            <li class="list-li">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. </li>
                        </ul>
                    </div>

                    <h3 class="resume-title">Education</h3>
                    <div class="resume-item">
                        <h4>Contrary to popular belief, Lorem Ipsum</h4>
                        <p>Contrary to popular belief, Lorem Ipsum</p>
                    </div>
                   
                </div>
                <div class="col-lg-6">
                    <h3 class="resume-title">Profession</h3>
                    <div class="resume-item">
                        <ul>
                            <li class="list-li">Contrary to popular belief, Lorem Ipsum</li>
                          </ul>
                    </div>
                    <h3 class="resume-title">Achievement</h3>
                    <div class="resume-item">
                        <ul>
                            <li class="list-li">Contrary to popular belief, Lorem Ipsum</li>
                            <li class="list-li">Contrary to popular belief, Lorem Ipsum</li>
                            <li class="list-li">Contrary to popular belief, Lorem Ipsum</li>
                            <li class="list-li">Contrary to popular belief, Lorem Ipsum</li>
                        </ul>
                    </div>
                </div>
              
              
            </div>
            <div class="text-center mt-4">
                <a href="#" download="#" class="btn btn-sm btn-primary"><i class="mdi mdi-file-document">Download .docx</i></a>&nbsp;&nbsp;
                <a href="#" download="#" class="btn btn-sm btn-danger"><i class="mdi mdi-file-pdf">Download .pdf</i></a>
            </div>
        </div>
    </section>