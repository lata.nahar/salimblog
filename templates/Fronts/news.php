<section class="section header-img"
             style="background-image: url(uploads/header/BpkxsyxLCueBbZlfiPKnMH.png)">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 page-next-level text-center">
                    <h4 class="pt-3 BebasNeue">News</h4>
                    <div class="page-next text-light"><a href="index.html">Home</a>/<span>News</span></div>
                </div>
            </div>
        </div>
    </section>
    <!-- home end -->
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="BebasNeue text-blue title">
                        Top News</h3>
                </div>
            </div>
            <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="single-blog-post">
                                <div class="blog-image ">
                                    <a href="https://albd.org/bn/articles/news/35696/একযুগের-পথচলা%">
                                        <img src="uploads/media/IOsDDWrNeBaHIzOYtHOK.jpg" class="img-fluid"
                                             alt="image">
                                    </a>

                                    <div class="date montserrat-bold">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-calendar">
                                            <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                            <line x1="16" y1="2" x2="16" y2="6"></line>
                                            <line x1="8" y1="2" x2="8" y2="6"></line>
                                            <line x1="3" y1="10" x2="21" y2="10"></line>
                                        </svg>
                                        January 11, 2021

                                    </div>
                                </div>

                                <div class="blog-post-content">
                                    <h3><a href="https://albd.org/bn/articles/news/35696/একযুগের-পথচলা%" class="text-break"
                                           >Lorem Ipsum is simply dummy text</a></h3>
                                    <span>
                                    <img src="uploads/favicon/WbgdOxAKQRQkKHZIuYNj.png" class="img-fluid"
                                    style="height: 18px;    width: 18px;    object-fit: contain;" alt="">
                                    <a style="margin-left: 5px" href="javascript:void(0)">Bangladesh Awami League</a></span>

                                    <p style="font-size: 12px">Lorem Ipsum is simply dummy text of the printing
                                        ..</p>
                                    <a href="#" target="_blank"
                                       class="read-more-btn montserrat-bold">Read More
                                        <svg xmlns="#" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-arrow-right">
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                            <polyline points="12 5 19 12 12 19"></polyline>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                           <div class="col-lg-4 col-md-4">
                            <div class="single-blog-post">
                                <div class="blog-image ">
                                    <a href="https://albd.org/bn/articles/news/35696/একযুগের-পথচলা%">
                                        <img src="uploads/media/IOsDDWrNeBaHIzOYtHOK.jpg" class="img-fluid"
                                             alt="image">
                                    </a>

                                    <div class="date montserrat-bold">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-calendar">
                                            <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                            <line x1="16" y1="2" x2="16" y2="6"></line>
                                            <line x1="8" y1="2" x2="8" y2="6"></line>
                                            <line x1="3" y1="10" x2="21" y2="10"></line>
                                        </svg>
                                        January 11, 2021

                                    </div>
                                </div>

                                <div class="blog-post-content">
                                    <h3><a href="https://albd.org/bn/articles/news/35696/একযুগের-পথচলা%" class="text-break"
                                           >Lorem Ipsum is simply dummy text</a></h3>
                                    <span>
                                    <img src="uploads/favicon/WbgdOxAKQRQkKHZIuYNj.png" class="img-fluid"
                                    style="height: 18px;    width: 18px;    object-fit: contain;" alt="">
                                    <a style="margin-left: 5px" href="javascript:void(0)">Bangladesh Awami League</a></span>

                                    <p style="font-size: 12px">Lorem Ipsum is simply dummy text of the printing
                                        ..</p>
                                    <a href="#" target="_blank"
                                       class="read-more-btn montserrat-bold">Read More
                                        <svg xmlns="#" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-arrow-right">
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                            <polyline points="12 5 19 12 12 19"></polyline>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="single-blog-post">
                                <div class="blog-image ">
                                    <a href="https://albd.org/bn/articles/news/35696/একযুগের-পথচলা%">
                                        <img src="uploads/media/IOsDDWrNeBaHIzOYtHOK.jpg" class="img-fluid"
                                             alt="image">
                                    </a>

                                    <div class="date montserrat-bold">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-calendar">
                                            <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                            <line x1="16" y1="2" x2="16" y2="6"></line>
                                            <line x1="8" y1="2" x2="8" y2="6"></line>
                                            <line x1="3" y1="10" x2="21" y2="10"></line>
                                        </svg>
                                        January 11, 2021

                                    </div>
                                </div>

                                <div class="blog-post-content">
                                    <h3><a href="https://albd.org/bn/articles/news/35696/একযুগের-পথচলা%" class="text-break"
                                           >Lorem Ipsum is simply dummy text</a></h3>
                                    <span>
                                    <img src="uploads/favicon/WbgdOxAKQRQkKHZIuYNj.png" class="img-fluid"
                                    style="height: 18px;    width: 18px;    object-fit: contain;" alt="">
                                    <a style="margin-left: 5px" href="javascript:void(0)">Bangladesh Awami League</a></span>

                                    <p style="font-size: 12px">Lorem Ipsum is simply dummy text of the printing
                                        ..</p>
                                    <a href="#" target="_blank"
                                       class="read-more-btn montserrat-bold">Read More
                                        <svg xmlns="#" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-arrow-right">
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                            <polyline points="12 5 19 12 12 19"></polyline>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>                 
                
            </div>
        </div>
    </section>

    <section class="section bg-light">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="BebasNeue text-blue text-center title">
                        More News</h3>
                </div>
            </div>
            <div class="infinite-scroll">
                <div class="row">
                        <div class="col-md-6">
                                <div class="card mb-3 border-0 shadow-sm">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            <img src="uploads/media/WzpCrXEfUcnneVrRujJF.jpg"
                                                 class="card-img horizontal-img" alt="...">
                                        </div>
                                        <div class="col-md-8 align-self-center">
                                            <div class="card-body hz-card">
                                                <span style="border-radius: 0" class="badge badge-primary mdi mdi-clock"> September 29, 2020</span>
                                                <a class="text-dark" href="#"> <h5 class="card-title text-break mt-1" style="margin-bottom: -5px">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5></a>
                                                <img src="frontEnd/images/favicon.png" class="img-fluid"
                                                style="height: 15px;   width: 18px;   object-fit: contain;" alt="">
                                                <a href="javascript:void(0)" style="font-size: 12px;margin-left: 4px;">Solaiman Salim</a>

                                                <p class="card-text mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                                    ..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                     <div class="col-md-6">
                                <div class="card mb-3 border-0 shadow-sm">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            <img src="uploads/media/WzpCrXEfUcnneVrRujJF.jpg"
                                                 class="card-img horizontal-img" alt="...">
                                        </div>
                                        <div class="col-md-8 align-self-center">
                                            <div class="card-body hz-card">
                                                <span style="border-radius: 0" class="badge badge-primary mdi mdi-clock"> September 29, 2020</span>
                                                <a class="text-dark" href="#"> <h5 class="card-title text-break mt-1" style="margin-bottom: -5px">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5></a>
                                                <img src="frontEnd/images/favicon.png" class="img-fluid"
                                                style="height: 15px;   width: 18px;   object-fit: contain;" alt="">
                                                <a href="javascript:void(0)" style="font-size: 12px;margin-left: 4px;">Solaiman Salim</a>

                                                <p class="card-text mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                                    ..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                 <div class="col-md-6">
                                <div class="card mb-3 border-0 shadow-sm">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            <img src="uploads/media/WzpCrXEfUcnneVrRujJF.jpg"
                                                 class="card-img horizontal-img" alt="...">
                                        </div>
                                        <div class="col-md-8 align-self-center">
                                            <div class="card-body hz-card">
                                                <span style="border-radius: 0" class="badge badge-primary mdi mdi-clock"> September 29, 2020</span>
                                                <a class="text-dark" href="#"> <h5 class="card-title text-break mt-1" style="margin-bottom: -5px">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5></a>
                                                <img src="frontEnd/images/favicon.png" class="img-fluid"
                                                style="height: 15px;   width: 18px;   object-fit: contain;" alt="">
                                                <a href="javascript:void(0)" style="font-size: 12px;margin-left: 4px;">Solaiman Salim</a>

                                                <p class="card-text mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                                    ..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>            <div class="col-md-6">
                                <div class="card mb-3 border-0 shadow-sm">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            <img src="uploads/media/WzpCrXEfUcnneVrRujJF.jpg"
                                                 class="card-img horizontal-img" alt="...">
                                        </div>
                                        <div class="col-md-8 align-self-center">
                                            <div class="card-body hz-card">
                                                <span style="border-radius: 0" class="badge badge-primary mdi mdi-clock"> September 29, 2020</span>
                                                <a class="text-dark" href="#"> <h5 class="card-title text-break mt-1" style="margin-bottom: -5px">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5></a>
                                                <img src="frontEnd/images/favicon.png" class="img-fluid"
                                                style="height: 15px;   width: 18px;   object-fit: contain;" alt="">
                                                <a href="javascript:void(0)" style="font-size: 12px;margin-left: 4px;">Solaiman Salim</a>

                                                <p class="card-text mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                                    ..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                    
                                                    
                 <div class="col-lg-12 col-md-12 d-flex justify-content-center">
                        <nav>
        <ul class="pagination">
            
                            <li class="page-item disabled" aria-disabled="true" aria-label="&laquo; Previous">
                    <span class="page-link" aria-hidden="true">&lsaquo;</span>
                </li>
            
            
                            
                
                
                                                                                        <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                                                                                                <li class="page-item"><a class="page-link" href="news4658.html?page=2">2</a></li>
                                                                                                <li class="page-item"><a class="page-link" href="news9ba9.html?page=3">3</a></li>
                                                                                                <li class="page-item"><a class="page-link" href="newsfdb0.html?page=4">4</a></li>
                                                                                                <li class="page-item"><a class="page-link" href="newsaf4d.html?page=5">5</a></li>
                                                                        
            
                            <li class="page-item">
                    <a class="page-link" href="news4658.html?page=2" rel="next" aria-label="Next &raquo;">&rsaquo;</a>
                </li>
                    </ul>
    </nav>

                    </div>
                </div>
            </div>

        </div>

    </section>