<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $bgimages
 */
?>
<div class="bgimages index content">
    <?= $this->Html->link(__('New Bgimage'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Bgimages') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('image') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($bgimages as $bgimage): ?>
                <tr>
                    <td><?= $this->Number->format($bgimage->id) ?></td>
                    <td><img src="../<?php echo h($bgimage->image)  ?>" alt="<?php echo h($bgimage->image)?>" width="250" height="150" ></td>
                    <td><?= h($bgimage->created) ?></td>
                    <td><?= h($bgimage->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $bgimage->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bgimage->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bgimage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bgimage->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
