<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $bgimage
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bgimage->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bgimage->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Bgimages'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="bgimages form content">
            <?= $this->Form->create($bgimage, ['type'=>'file']) ?>
            <fieldset>
                <legend><?= __('Edit Bgimage') ?></legend>
                <?php
                    echo $this->Form->control('bg_image',['type'=>'file']);  
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
