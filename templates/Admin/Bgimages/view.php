<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $bgimage
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Bgimage'), ['action' => 'edit', $bgimage->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Bgimage'), ['action' => 'delete', $bgimage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bgimage->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Bgimages'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Bgimage'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="bgimages view content">
            <h3><?= h($bgimage->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Image') ?></th>
                    <td><?= h($bgimage->image) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($bgimage->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($bgimage->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($bgimage->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
