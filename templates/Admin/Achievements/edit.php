<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Achievement $achievement
 */
?>
<div class="row" style="margin-top:20px;">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $achievement->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $achievement->id), 'class' => 'link-color']
        
            ) ?>
            <?= $this->Html->link(__('List Achievements'), ['action' => 'index'], ['class' => 'link-color']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="achievements form content">
            <?= $this->Form->create($achievement, ['type'=>'file']) ?>
            <fieldset>
                <legend><?= __('Edit Achievement') ?></legend>
                <?php
                    echo $this->Form->control('title');
                    echo $this->Form->control('description');
                   $path = h($achievement->image);
                    echo"<label>Status</label>";
                    echo $this->Form->select('status', [
                        'active' => 'active',
                        'inactive' => 'inactive',
                    ]);
                    echo $this->Form->control('change_image',['type'=>'file','label'=>'Image']);
                    echo '<small class="form-text text-muted" >Image dimention 600 X 600</small>';
                    echo "<br></br>";                
                ?>
               
                <!-- <img src="../<?php echo h($achievement->image)  ?>" alt="<?php echo h($achievement->image)  ?>" width="100" height="100" > -->
                
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?php echo "<br></br>";?>
            <?= $this->Flash->render() ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
