<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Achievement $achievement
 */
?>
<div class="row" style="margin-top:80px;">
    <aside class="column">
        <div class="side-nav"> 
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Achievements'), ['action' => 'index'], ['class' => 'link-color']) ?> 
      </div> 
    </aside>
    <div class="column-responsive column-80">
        <div class="achievements form content">
        
            
            <?= $this->Form->create($achievement,['type'=>'file']) ?>
            <fieldset>
                <legend><?= __('Add Achievement') ?></legend>
                
                <!-- <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title"  placeholder="Enter Title">
                    
                </div>
                <div class="form-group">
                    <label for="discription">Discription</label>
                    <input type="text" class="form-control" id="discription"  placeholder="Enter discription">
                    
                </div>

                <div class="form-group">
                    <label for="status">Select Status</label>
                    <select class="form-control" id="status">
                    <option value="active">active</option>
                    <option value="inactive">inactive</option>
     
                </select>
                </div>

                <div class="form-group">
                    <label for="image_file">Input Image</label>
                    <input type="file" class="form-control-file" id="image_file">
                    <small id="image_file" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div> -->
                 
                 <?php
                
                    echo $this->Form->control('title');
                    echo $this->Form->control('description');
                   // echo $this->Form->control('image');
                   echo '<label >Select Status</label>';
                    echo $this->Form->select('status', [
                        'active' => 'active',
                        'inactive' => 'inactive',
                        
                    ]);

                    echo $this->Form->control('image_file',['type'=>'file', 'label' => 'Image']);
                    echo '<small class="form-text text-muted" >Image dimention 600 X 600</small>';
                    echo "<br></br>";
                  
                ?> 
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?php echo "<br></br>";?>
            <?= $this->Flash->render() ?>
            
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
