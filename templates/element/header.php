<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="frontEnd/images/pic.png">
    <title>Solaiman Salim</title>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <meta property="og:image" content="frontEnd/images/slider/large-banner-b.png"/>
    <meta property="og:site_name" content="Zunaid Ahmed Solaiman"/>
    <meta property="og:description" content=""/>
    <!-- Bootstrap core CSS -->
    <link href="frontEnd/css/bootstrap.min.css" rel="stylesheet">
    <meta name="csrf-token" content="IYRE2DQQ8ZDddeV2cbgVEwfxuJfIzupzdmRNPCnl"/>
    <!-- Bootstrap core CSS -->
    <link href="frontEnd/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="frontEnd/css/mobiriseicons.css" rel="stylesheet">
    <link href="frontEnd/css/animate.min.css" rel="stylesheet">

    <!--Magnific popup -->
    <link rel="stylesheet" type="text/css" href="frontEnd/css/magnific-popup.css"/>
    <link rel="stylesheet" type="text/css"
          href="frontEnd/plugin/fancybox-master/dist/jquery.fancybox.min.css"/>
    <link rel="stylesheet" href="../code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!--Slider-->
    <link href="frontEnd/plugin/owl-2.3/dist/assets/owl.carousel.css" rel="stylesheet">
    <link href="frontEnd/plugin/owl-2.3/dist/assets/owl.theme.default.css" rel="stylesheet">
    <link rel="stylesheet" href="frontEnd/css/flickity.min.css">
    <link rel="stylesheet" href="frontEnd/css/assets/build/style.min.css"   media="all">

<!-- Custom styles for this template -->
    <link href="frontEnd/css/menu.css" rel="stylesheet">
    <link href="frontEnd/css/style.css" rel="stylesheet">
    <!--Template Color-->
    <link href="frontEnd/css/colors/default.css" rel="stylesheet">
        <style>
        .ini-img {
            height: 110px;
            object-fit: contain;
        }

        .media-img img {
            width: 85px !important;
        }

        .is-selected {
            opacity: 1 !important;
        }

        /* smaller, dark, rounded square */
        .flickity-button {
            background: #3d8ede;
        }

        .flickity-button:hover {
            background: #1f73c5;
        }

        .flickity-prev-next-button {
            width: 40px;
            height: 40px;
            border-radius: 5px;
        }

        /* icon color */
        .flickity-button-icon {
            fill: white;
        }

        .bb-pt {
            padding-top: 60px;
        }

        .ex img {
            display: block;
            height: 450px;
            object-fit: cover;
            /*object-position: top;*/
        }


        @media ( max-width: 768px ) {
            .ex img {
                height: 200px !important;
                object-fit: cover;
            }

            .floating-segment {
                margin-top: -75px !important;
            }
        }

        .carousel-cell {
            width: 60%;
            /*margin-right: 8px;*/
            /*border-radius: 2px;*/
            opacity: 0.2;
            counter-increment: carousel-cell;
        }

        .flickity-viewport {
            background-color: black;
        }

        /* cell number */
        .carousel-cell:before {
            display: block;
            content: counter(carousel-cell);
        }

        .floating-segment {
            position: relative;
            margin-top: -120px;
        }

        /* .floating-segment img{
             height: 60px;
             object-fit: cover;
         }*/
        .text-break {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 2; /* number of lines to show */
            -webkit-box-orient: vertical;
        }
    </style>
    <style>
        @media (max-width: 768px) {
            #involved-btn{
                display: block !important;
                text-align: center !important;
            }
        }
    </style>
</head>
